public class Teacher extends Human{


  public Teacher(String name, int age) {
    super(name, age);
  }

  public void checkHomework() {
    System.out.println("Teacher checking homework");
  }
}
