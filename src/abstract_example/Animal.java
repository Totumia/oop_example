package abstract_example;

public abstract class Animal {
  public String nickname;
  public int age;
  public Animal(String nickname,int age){
    this.nickname = nickname;
    this.age = age;
  }
  public abstract void makeSound();
}
