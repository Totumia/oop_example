package abstract_example;

public class Cat extends Animal {
  public Cat(String nickname, int age) {
    super(nickname, age);
  }

  @Override
  public void makeSound() {
    System.out.println("Meow");
  }
}
