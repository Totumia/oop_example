package abstract_example;

public class AnimalRunner {
  public static void main(String[] args) {
    Dog dog = new Dog("dog",3);
    dog.makeSound();
    Cat cat = new Cat("Traviskot",2);
    cat.makeSound();
  }
}
