package abstract_example;

public class Dog extends Animal{
  public Dog(String nickname, int age) {
    super(nickname, age);
  }

  @Override
  public void makeSound() {
    System.out.println("hav hav");
  }
}
