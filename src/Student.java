public class Student extends Human {

  public Student(String name, int age) {
    super(name, age);
  }

  public void doHomework() {
    System.out.println("Student do homework");
  }
}
